// layouts
import HeaderOnly from '~/components/Layouts/HeaderOnly';

// pages
import Home from '~/pages/Home';
import Following from '~/pages/Following';
import Profile from '~/pages/Profile';
import Upload from '~/pages/Upload';
import Search from '~/pages/Search';

const publicRoutes = [
  { path: '/', component: Home },
  { path: '/search', component: Search, layout: null },
];
const privateRoutes = [
  { path: '/following', component: Following },
  { path: '/profiles', component: Profile },
  { path: '/upload', component: Upload, layout: HeaderOnly },
];

export { publicRoutes, privateRoutes };
