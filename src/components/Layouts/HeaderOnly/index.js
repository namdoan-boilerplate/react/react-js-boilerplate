import React from 'react';
import Header from '../partials/Header';

function HeaderOnly({ children }) {
  return (
    <div>
      <Header />
      {children}
    </div>
  );
}

export default HeaderOnly;
