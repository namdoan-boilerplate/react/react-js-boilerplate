import React from 'react';
import Header from '../partials/Header';
import Sidebar from '../partials/Sidebar';

function DefaultLayout({ children }) {
  return (
    <div>
      <Header />
      <Sidebar />
      {children}
    </div>
  );
}

export default DefaultLayout;
